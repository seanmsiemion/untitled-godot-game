extends Node

export var playerScene:PackedScene = preload("res://Elements/Player/Player.tscn")
export var gameScene:PackedScene

signal playerConnected(player)
signal playerDisconnected(player)
signal gameReady()
signal allPlayersReady(ready)
var allPlayersReady:bool = false

var players = {} #id(int): playerScene(node)
var thisPlayer:Player = null

var canScore:bool = false #I hate this, it's to game specific...


enum SPAWN_LOCATION {
	UNDEFINED=-1,
	UPPER_LEFT,
	LOWER_RIGHT,
	UPPER_RIGHT,
	LOWER_LEFT
}#I don't like this, trying to fix a godot loading sequnce bug

func _ready():
	NetworkManager.connect("lobbyReady",self,"lobbyReady")
	NetworkManager.connect("peerConnected",self,"peerConnected")
	NetworkManager.connect("peerDisconnected",self,"peerDisconnected")
	NetworkManager.connect("networkDropped",self,"networkDropped")
	NetworkManager.connect("localGame",self,"localGame")
	NetworkManager.connect("kicked",self,"kicked")
	
func networkDropped():
	for i in players.keys():
		peerDisconnected(i)
	
	
func localGame():
	pass
	
func lobbyReady():
	makePlayer(get_tree().get_network_unique_id())
	emit_signal("gameReady")

func peerConnected(id):
	var player = makePlayer(id)
	if is_network_master():
		pass #setup client
#	emit_signal("playerConnected",player)

func peerDisconnected(id):
	var p = players[id]
	emit_signal("playerDisconnected",p)
	p.queue_free()
	players.erase(id)

func makePlayer(id)->Player:
	var p = playerScene.instance()
	p.set_network_master(id)
	players[id] = p
	add_child(p)
	if p.is_network_master(): #i don't like this hear, but I had issues not doing this
		thisPlayer = p
	p.connect("dataUpdated",self,"playerDataUpdate")
	emit_signal("playerConnected",p)
	return p

puppet func setLevel(levelString):
	add_child(load(levelString).instance())


func getPlayer(id)->Player:
	return players[id]

func hasMinPlayerCount():
	return players.size() >= 1

func beginGameStart():
	rpc("setUpGame")
	NetworkManager.lockGame()
	for i in players:
		players[i].data.ready = false

puppetsync func setUpGame():
	thisPlayer.resetPlayer()
	get_tree().change_scene_to(gameScene)

master func loaded():
	get_tree().get_rpc_sender_id()

func gameEnd():
	rpc("resetMainMenu")
	NetworkManager.unlockGame()
#	assert(false, "not implemented")

puppetsync func resetMainMenu():
	thisPlayer.resetPlayer()
	get_tree().change_scene(NetworkManager.mainMenu)

func playerDataUpdate(data):
	allPlayersReady = true
	for i in players:
		allPlayersReady = players[i].data.ready and allPlayersReady
	emit_signal("allPlayersReady",allPlayersReady)

func kick(player:Player):
	NetworkManager.kick(player.get_network_master())

func kicked():
	pass
