extends Node


func _ready():
	yield(self,"ready")
	for i in OS.get_cmdline_args():
		match i:
			"-c","--client":
				fastJoin()
			"-h","--host":
				pass


func fastJoin():
	$"../joinHostMenu"._on_random_pressed()
	$"../joinHostMenu"._on_ConnectLocal_pressed()
	yield(NetworkManager,"lobbyReady")
	$"../lobbyMenu".readyUp()
	yield(NetworkManager,"networkDropped")
	get_tree().quit() #todo, this fails because this node doesn't exist any more. todo fix me
