#!/bin/bash

targetPath="./addons/GreenFoxsProjectPlugin/gitLog.gd"

file()
{
cat << EOF
class_name GitCommitLog
extends Node

func _ready():
	pause_mode = Node.PAUSE_MODE_PROCESS
	
func _input(event):
	if event is InputEventKey and event.pressed and event.scancode == KEY_L and event.control and event.shift:
			print_debug(git_log)
	
const git_log: = """
$(git log)
"""
EOF
}


file > $targetPath
